﻿using Microsoft.Win32.TaskScheduler;
using OVRTT.Common;
using System.IO;
using System.Windows;
using System.Windows.Media;
using Valve.VR;

namespace OVRTTools.Launch
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class wndMain : Window
    {

        public wndMain()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Find our Task from the Task Scheduler.
            var task = TaskService.Instance.FindTask(OVRTTConsts.OVRTT_UAC_TASK_ID);

            // YOU IDIOT
            if (task == null) {
                tbPleaseWait.Text = "You need to run OVRTTools.Configure and install the UAC Bypass first.";
                this.Background = Brushes.DarkRed;
                this.Focus();
                return;
            }

            // I have no fucking clue why this is necessary, but the launcher does it, so I'm not about to rock the boat.
            EVRInitError peError = EVRInitError.None;
            OpenVR.Init(ref peError, EVRApplicationType.VRApplication_Utility);
            if (peError == EVRInitError.None)
            {
                OpenVR.Applications.RemoveApplicationManifest(Path.Join(((ExecAction)task.Definition.Actions[0]).WorkingDirectory, "manifest.vrmanifest"));
            }

            // Steam init goes here, that's deffo not needed though.

            // Fire our sneaky breeki task.
            task.Run();

            // And bail!
            Close();
        }
    }
}
