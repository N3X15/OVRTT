﻿using SharpYaml.Serialization;
using System;
using System.IO;

namespace OVRTT.Common
{
    [Serializable]
    public class Configuration
    {
        public class InvalidConfigException: Exception { }

        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        // DDMMYYYY
        public const uint CURRENT_VERSION = 15012020;
        /// <summary>
        /// Version of the config file.
        /// </summary>
        public uint Version { get; set; }
        /// <summary>
        /// OVR Toolkit-related stuff
        /// </summary>
        public OVRTConfig OVRT { get; set; }

        public Configuration()
        {
            Version = CURRENT_VERSION;
            OVRT = new OVRTConfig();
        }

        public static Configuration Load(string filename)
        {
            var cfg = new Configuration();
            var settings = new SerializerSettings();
            settings.RegisterAssembly(typeof(Configuration).Assembly);
            try
            {
                cfg = (new Serializer(settings)).Deserialize<Configuration>(File.ReadAllText(filename));
            } 
            catch(Exception e)
            {
                Logger.Error(e, "Failed to load configuration, loading fallback.");
            }
            return cfg;
        }

        public static void Save(Configuration cfg, string filename)
        {
            var settings = new SerializerSettings();
            settings.RegisterAssembly(typeof(Configuration).Assembly);
            settings.DefaultStyle = SharpYaml.YamlStyle.Block;
            settings.EmitTags = false;
            var ser = new Serializer(settings);
            File.WriteAllText(filename, ser.Serialize(cfg));
        }
    }
}
