﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ValveKeyValue;

namespace OVRTT.Common
{
    public class Steam
    {
        public static readonly string DefaultInstall = @"C:\Program Files (x86)\Steam\";
        public static readonly string DefaultLibrary = @"C:\Program Files (x86)\Steam\steamapps";
        public static readonly string LibraryFile = @"libraryfolders.vdf";

        public static List<string> Libraries = new List<string>();
        public static Dictionary<uint, String> AppInstalls = new Dictionary<uint, string>();

        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private static void LoadLibraries()
        {
            if (Libraries.Count > 0) 
                return;
            Libraries.Add(DefaultInstall);
            KVObject kvo;
            using (var stream = File.OpenRead(Path.Combine(DefaultInstall, "steamapps", LibraryFile)))
            {
                kvo = KVSerializer.Create(KVSerializationFormat.KeyValues1Text).Deserialize(stream);
            }
            foreach (var kvp in kvo.Children)
            {
                uint id = 0;
                if(!uint.TryParse(kvp.Name, out id))
                {
                    continue;
                }
                Libraries.Add(kvp.Value.ToString());
            }
            string libpath;
            foreach(var _libpath in Libraries)
            {
                libpath = Path.Join(_libpath, "SteamApps", "common");
                foreach (var gamedir in Directory.EnumerateDirectories(libpath))
                {
                    //gamedir = Path.Join(libpath, _gamedir);
                    LoadAppIDFromDir(gamedir);
                }
            }
        }

        private static void LoadAppIDFromDir(string gamedir)
        {
            if (File.Exists(Path.Join(gamedir, "steam_appid.txt")))
            {
                var appid = uint.Parse(File.ReadAllText(Path.Join(gamedir, "steam_appid.txt")).Trim());
                AppInstalls[appid] = gamedir;
                System.Diagnostics.Debug.WriteLine(string.Format("{0} {1}", appid, gamedir));
            }
        }

        public static string? FindAppInstall(uint appid)
        {
            LoadLibraries();
            if (!AppInstalls.ContainsKey(appid))
                return null;
            return AppInstalls[appid];
        }
    }
}
