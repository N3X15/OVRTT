﻿using SharpYaml.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OVRTT.Common
{
    [Serializable]
    public class OVRTConfig
    {
        /// <summary>
        /// Executable to run in WorkingDir when launching OVR Toolkit.
        /// </summary>
        public string Executable { get; set; }

        /// <summary>
        /// Working directory
        /// </summary>
        public string WorkingDir { get; set; }

        public OVRTConfig()
        {
            Executable = "";
            WorkingDir = "";
        }

        public void Detect()
        {
            var installdir = Steam.FindAppInstall(1068820);
            WorkingDir = installdir ?? "";
            Executable = installdir == null ? "" : Path.Join(installdir, "OVR Toolkit.exe");
        }
    }
}
