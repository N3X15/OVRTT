﻿using Microsoft.Win32;
using Microsoft.Win32.TaskScheduler;
using Ookii.Dialogs.Wpf;
using OVRTT.Common;
using SharpYaml;
using SharpYaml.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace OVRTTools.Configure
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class wndMain : Window
    {
        private string ConfigPath;
        private Configuration Config;
        private bool Validated;
        private bool HasLoaded=false;

        public wndMain()
        {
            InitializeComponent();
            ConfigPath = System.IO.Path.Join(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "config.yml");
            Config = Configuration.Load(ConfigPath);
            Loaded += WndMain_Loaded;
        }

        private void WndMain_Loaded(object sender, RoutedEventArgs e)
        {
            HasLoaded = true;
            ReadValues();
            validateUninstallTask();
        }

        private bool taskExists()
        {
            return TaskService.Instance.RootFolder.EnumerateTasks().Any((t) => t.Name == OVRTTConsts.OVRTT_UAC_TASK_ID);
        }
        private void validateUninstallTask()
        {
            cmdUninstallTask.IsEnabled = taskExists();
        }

        private bool Validate()
        {
            if (!HasLoaded) return true;
            Validated = true;
            mustBeDirectory(null, txtOVRTWDPath);
            mustBeFile(null, txtOVRTExePath);
            if (Validated)
            {
                WriteValues();
            }
            cmdInstallTask.IsEnabled = Validated;
            return Validated;
        }

        private void mustBeDirectory(Label lbl, TextBox txt)
        {
            if (Directory.Exists(txt.Text))
                RevalidateTextBox(lbl, txt);
            else
            {
                InvalidateTextBox(lbl, txt);
                Validated = false;
            }
        }

        private void mustBeFile(Label lbl, TextBox txt)
        {
            if (File.Exists(txt.Text))
                RevalidateTextBox(lbl, txt);
            else
            {
                InvalidateTextBox(lbl, txt);
                Validated = false;
            }
        }

        private void InvalidateTextBox(Label lbl, TextBox txt)
        {
            txt.Background = Brushes.White;
            txt.Foreground = Brushes.Red;
        }

        private void RevalidateTextBox(Label lbl, TextBox txt)
        {
            txt.Background = Brushes.White;
            txt.Foreground = Brushes.Green;
        }

        private void WriteValues()
        {
            Config.OVRT.Executable = txtOVRTExePath.Text;
            Config.OVRT.WorkingDir = txtOVRTWDPath.Text;
            Configuration.Save(Config, ConfigPath);
        }

        private void ReadValues()
        {
            HasLoaded = false;
            txtOVRTExePath.Text = Config.OVRT.Executable;
            txtOVRTWDPath.Text = Config.OVRT.WorkingDir;
            HasLoaded = true;
        }

        private void cmdDetectOVRTExe_Click(object sender, RoutedEventArgs e)
        {
            LocateOVRT();
        }

        private void LocateOVRT()
        {
            Config.OVRT.Detect();
            txtOVRTExePath.Text = Config.OVRT.Executable;
            txtOVRTWDPath.Text = Config.OVRT.WorkingDir;
            Validate();
        }

        private void cmdBrowseOVRTExe_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new VistaOpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.Multiselect = false;
            ofd.Filter = "OVR Toolkit.exe|OVR Toolkit.exe";
            if(ofd.ShowDialog() == true)
            {
                txtOVRTExePath.Text = ofd.FileName;
                txtOVRTWDPath.Text = System.IO.Path.GetDirectoryName(txtOVRTExePath.Text);
                Validate();
            }
        }

        private void txtOVRTExePath_TextChanged(object sender, TextChangedEventArgs e)
        {
            Validate();
        }

        private void txtOVRTWDPath_TextChanged(object sender, TextChangedEventArgs e)
        {
            Validate();
        }

        private void cmdBrowseOVRTWD_Click(object sender, RoutedEventArgs e)
        {

            var fbd = new VistaFolderBrowserDialog();
            fbd.ShowNewFolderButton = false;
            if (fbd.ShowDialog() == true)
            {
                txtOVRTWDPath.Text = fbd.SelectedPath;
                Validate();
            }
        }

        private void cmdInstallTask_Click(object sender, RoutedEventArgs e)
        {
            var ts = TaskService.Instance;

            var task = ts.FindTask(OVRTTConsts.OVRTT_UAC_TASK_ID);
            var taskDef = (task == null) ? ts.NewTask() : task.Definition;
            taskDef.RegistrationInfo.Author = "OVR Toolkit Tools";
            taskDef.RegistrationInfo.Description = "Bypasses UAC prompts when launching OVR Toolkit.";
            taskDef.Settings.Compatibility = TaskCompatibility.V2_3;
            taskDef.Principal.RunLevel = TaskRunLevel.Highest;
            taskDef.Settings.AllowDemandStart = true;
            taskDef.Settings.MultipleInstances = TaskInstancesPolicy.Parallel;
            //taskDef.Settings.RunOnlyIfLoggedOn = true;
            taskDef.Settings.RunOnlyIfIdle = false;
            taskDef.Settings.StopIfGoingOnBatteries = false;
            taskDef.Settings.DisallowStartIfOnBatteries = false;
            taskDef.Actions.Add(Config.OVRT.Executable, "-batchmode", Config.OVRT.WorkingDir);
            if(task == null)
                ts.RootFolder.RegisterTaskDefinition(OVRTTConsts.OVRTT_UAC_TASK_ID, taskDef);

            // Patch files
            var source = Path.Join(Config.OVRT.WorkingDir, "OVRToolkit-Launcher.exe");
            var dest = Path.Join(Config.OVRT.WorkingDir, "OVRToolkit-Launcher.exe.bak");
            // OVRToolkit-Launcher2.exe
            var signature = new byte[] { 0x4F, 0x56, 0x52, 0x54, 0x6F, 0x6F, 0x6C, 0x6B, 0x69, 0x74, 0x2D, 0x4C, 0x61, 0x75, 0x6E, 0x63, 0x68, 0x65, 0x72, 0x32, 0x2E, 0x65, 0x78, 0x65 };
            if (!File.Exists(dest) || Utils.FirstIndexOfSequence(File.ReadAllBytes(source), signature, 0) != null)
                File.Copy(source, dest, true);
            File.Copy("OVRTTools.Launch.exe", source, true);
            File.Copy("OVRTTools.Launch.runtimeconfig.json", Path.Join(Config.OVRT.WorkingDir, "OVRToolkit-Launcher.runtimeconfig.json"), true);
            File.Copy("OVRTTools.Launch.dll", Path.Join(Config.OVRT.WorkingDir, "OVRToolkit-Launcher.dll"), true);

            var toCopy = new [] {
                "Microsoft.Win32.TaskScheduler.dll",
                "OVRTT.Common.dll",
                "SharpYaml.dll",
                "Valve.VR.dll",
                "NLog.dll",
                "nlog.config",
                "ValveKeyValue.dll",
            };
            foreach(var basename in toCopy)
                File.Copy(basename, Path.Join(Config.OVRT.WorkingDir, basename), true);
            
            var ss = new SerializerSettings();
            ss.DefaultStyle = YamlStyle.Block;
            var ser = new Serializer();
            File.WriteAllText(Path.Join(Config.OVRT.WorkingDir, "ovrtt.manifest.yml"), ser.Serialize(toCopy));

            validateUninstallTask();

            MessageBox.Show("Task successfully added and patch installed.");
        }

        private void deleteIfPresent(string path)
        {
            path = Path.Join(Config.OVRT.WorkingDir, path);
            if (File.Exists(path))
                File.Delete(path);
        }

        private void cmdUninstallTask_Click(object sender, RoutedEventArgs e)
        {
            var ts = TaskService.Instance;
            if (taskExists())
                ts.RootFolder.DeleteTask(OVRTTConsts.OVRTT_UAC_TASK_ID);
            var source = Path.Join(Config.OVRT.WorkingDir, "OVRToolkit-Launcher.exe.bak");
            var dest = Path.Join(Config.OVRT.WorkingDir, "OVRToolkit-Launcher.exe");
            if (File.Exists(source))
                File.Move(source, dest, true);
            deleteIfPresent("OVRToolkit-Launcher.dll");
            deleteIfPresent("OVRToolkit-Launcher.deps.json");
            deleteIfPresent("OVRToolkit-Launcher.runtimeconfig.json");
            deleteIfPresent("OVRToolkit-Launcher.runtimeconfig.dev.json");

            var ser = new Serializer();
            var manifest = ser.Deserialize<string[]>(File.ReadAllText(Path.Join(Config.OVRT.WorkingDir, "ovrtt.manifest.yml")));
            string path;
            foreach(var basename in manifest)
            {
                path = Path.Join(Config.OVRT.WorkingDir, basename);
                if(File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            File.Delete(Path.Join(Config.OVRT.WorkingDir, "ovrtt.manifest.yml"));
            validateUninstallTask();
            MessageBox.Show("Task removed and patch uninstalled successfully.");
        }
    }
}
