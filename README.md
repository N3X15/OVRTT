# OVRTT - OVR Toolkit Tools

This tiny set of utilities lets you launch OVR Toolkit without needing to do drastic
things like *making Steam run as administrator* (breaks things) or *disabling UAC entirely* (just stupid).

OVRTT accomplishes its UAC bypass by creating a Task Scheduler task which launches OVRT
as an elevated process.  This is common practice when a developer wants to run an
elevated process in the background without spamming the user with UAC. (See: CCleaner)

## Installation

1. Extract a release .zip to some folder somewhere.
1. **Run `OVRTTools.Configure.exe` first.**
    1. Accept the UAC prompt, since you need the extra privileges to make the bypass task.
    1. Select the `Detect` button next to `Executable`.  
        * If that didn't work, `browse` to `OVR Toolkit.exe` manually, and then do the same with the `Working Directory`.
        * If you did it right, both textboxes will have green text.
    1. Press `Install UAC Bypass`.
        * You can verify this proceeded successfully if the `Uninstall UAC Bypass` button becomes enabled.
        * You can review the task by opening `Start > Task Scheduler`, clicking `Task Scheduler Library` in the left column, and looking for `OVRTT UAC Skip`. **Look, but don't touch!**
        * This will also overwrite the following files in your OVR Toolkit directory in order to facilitate calling the task when you try to launch OVR Toolkit:
            * `OVRToolkit-Launcher.exe`
1. Close `OVRTTools.Configure.exe`.

You should now be able to run OVR Toolkit without UAC prompts.

## Uninstall

If you want to go back to what you had originally:

1. Run `OVRTTools.Configure.exe` again
1. Click on `Uninstall UAC Bypass`.  This will restore changed files from backup, and remove OVRTT's own files from OVR Toolkit.
1. Close the configuration dialog.

## How it works
This is a basic theory-of-operation for the curious:

### UAC Bypass Installation
1. `OVRTTools.Configure.exe` creates or updates the `OVRTT UAC Skip` task.
1. `OVRTTools.Configure.exe` makes a backup of `OVRToolbox-Launcher.exe`, if needed.
1. `OVRToolbox-Launcher.exe` is replaced with `OVRTTools.Launch.exe`.
1. `OVRTTools.Configure.exe` copies over a selection of support DLLs
1. It also writes a manifest of what files need to be deleted on uninstall. (`ovrtt.manifest.yml`)

### OVR Toolkit Launch (Patched)
0. When running from SteamVR autolaunch:
    1. SteamVR executes `OVRToolbox-AutoLaunch.exe`
    1. `OVRToolbox-AutoLaunch.exe` asks Steam to run OVR Toolbox in non-VR mode.
    1. `OVRToolbox-AutoLaunch.exe` exits.
1. Steam executes `OVRToolbox-Launcher.exe`, which we've changed in the install process.
1. `OVRToolbox-Launcher.exe` initializes SteamVR, if need be.
1. `OVRToolbox-Launcher.exe` asks the Task Scheduler to run the `OVRTT UAC Skip` task.
1. The `OVRTT UAC Skip` task runs, and executes `OVR Toolbox.exe` with elevated privileges silently.

## Licensing
OVRTT is available under the terms of the MIT Open Source License.

## Compiling
You need Microsoft Visual Studio Community 2019 with .NET tooling for desktop applications enabled.

1. Run `git submodule update --init --recursive` to grab Valve's OpenVR libraries.
1. Open the solution
1. Fetch NuGet packages
1. Rebuild all

If you'd like to package, you also need to run `PACKAGE.bat`.
