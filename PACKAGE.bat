del /q /f dist
rmdir dist
mkdir dist
cd bin\netcoreapp3.1\
copy /Y Microsoft.Win32.TaskScheduler.dll ..\..\dist\
copy /Y nlog.config ..\..\dist\
copy /Y NLog.dll ..\..\dist\
copy /Y Ookii.Dialogs.Wpf.dll ..\..\dist\
copy /Y openvr_api.dll ..\..\dist\
copy /Y OVRTT.Common.dll ..\..\dist\
::copy /Y OVRTT.Common.deps.json ..\..\dist\
copy /Y OVRTTools.Configure.exe ..\..\dist\
copy /Y OVRTTools.Configure.dll ..\..\dist\
::copy /Y OVRTTools.Configure.deps.json ..\..\dist\
copy /Y OVRTTools.Configure.runtimeconfig.json ..\..\dist\
::copy /Y OVRTTools.Configure.runtimeconfig.dev.json ..\..\dist\
copy /Y OVRTTools.Launch.exe ..\..\dist\
copy /Y OVRTTools.Launch.dll ..\..\dist\
::copy /Y OVRTTools.Launch.deps.json ..\..\dist\
copy /Y OVRTTools.Launch.runtimeconfig.json ..\..\dist\
::copy /Y OVRTTools.Launch.runtimeconfig.dev.json ..\..\dist\
copy /Y SharpYaml.dll ..\..\dist\
copy /Y Valve.VR.dll ..\..\dist\
::copy /Y Valve.VR.deps.json ..\..\dist\
copy /Y ValveKeyValue.dll ..\..\dist\
robocopy de ..\..\dist\de\ /E
robocopy es ..\..\dist\es\ /E
robocopy fe ..\..\dist\fe\ /E
robocopy it ..\..\dist\it\ /E
robocopy pl ..\..\dist\pl\ /E
robocopy ru ..\..\dist\ru\ /E
robocopy zh-CN ..\..\dist\zh-CN\ /E
cd ..\..
